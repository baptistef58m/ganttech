<?php

    class Associer {

        public $id_assoc;
        public $id_associer;
        public $nom;

        public function __construct($assoc, $associer, $n){
			$this->id_assoc = $assoc;
            $this->id_associer = $associer;
            $this->nom = $n;
        }

        public function getId_assoc() {
            return $id_assoc;
        }
        public function setId_assoc($id_assoc) {
            $this->id_assoc = $id_assoc;
        }

        public function getId_associer() {
            return $id_associer;
        }
        public function setId_associer($id_associer) {
            $this->id_associer = $id_associer;
        }

        public function getNom() {
            return $nom;
        }
        public function setNom($nom) {
            $this->nom = $nom;
        }

        public function __toString() {
            return "{".
                    "id_assoc : $this->id_assoc, ".
                    "id_associer : $this->id_associer, ".
                    "nom : $this->nom, ".
                    '}</br>';
        }

        public static function listAssoc($con) {
            $list = array();
            
            $id_projet = $_SESSION['id_projet'];
            $sql = "SELECT a.id_assoc, a.id_associer, l.user FROM associer a INNER JOIN login l ON a.id_associer = l.id WHERE a.id_projet = $id_projet";
            $result = mysqli_query($con, $sql) or die("Erreur lors du chargement des données, réessayez.");
            while($ligne = mysqli_fetch_array($result)){
                $tache = new Associer($ligne['id_assoc'], $ligne['id_associer'], $ligne['user']);
                array_push($list, $tache);
            }
            return $list;
        }

        public function supp($bdd, $id){
            $sql = "DELETE FROM associer WHERE id_assoc = $id";
            $bdd->exec($sql);
        }
    }
?>