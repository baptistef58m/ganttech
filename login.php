<?php require_once "controle_donne.php"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Page de Connexion</title>
    <link rel="stylesheet" href="login.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="login">
                <form action="login.php" method="POST" autocomplete="" class="rectangle">
                    <h2 class="text-center">Connexion</h2>
                    <p class="text-center">Connectez-vous avec votre mail et votre mot de passe.</p>
                    <?php
                    if(count($errors) > 0){
                        ?>
                        <div class="Erreurs">
                            <?php
                            foreach($errors as $showerror){
                                echo $showerror;
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="text">
                        <input class="form-control" type="email" name="email" placeholder="Adresse Mail" required value="<?php echo $email ?>">
                    </div>
                    <div class="text">
                        <input class="form-control" type="password" name="mdp" placeholder="Mot de passe" required>
                    </div>
                    <div class="form-group">
                        <input class="bouton-connect" type="submit" name="login" value="Login">
                    </div>
                    <div class="creer-un-compte">Vous n'êtes pas membre ? <a href="creer_compte.php">Inscrivez-vous ! </a></div>
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>
