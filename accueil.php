<?php require_once "controle_donne.php";?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Page d'accueil</title>
    <link rel="stylesheet" href="accueil.css">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <nav>
        <div class="logo">
    GantTech</div>
    <input type="checkbox" id="click">
        <label for="click" class="menu-btn">
            <i class="fas fa-bars"></i>
        </label>
<ul>
    <li><a class="active" href="#">Home</a></li>
    <li><a href="accueil.php">Accueil</a></li>
    <li><a href="Contact.html">Nous Contacter</a></li>
    <li><a href="login.php">Déconnexion</a></li>
</ul>
</nav>
    <div class="container">
        <div class="row">
            <div class="accueil">
                <form action="accueil.php" method="POST" autocomplete="" class="rectangle">

                    <?php
                        $email = $_SESSION['email'];
                        //récup de l'id et du nom d'utilisateur grace à l'email.
                        $reponse = $bdd->query("SELECT * FROM login WHERE email = '$email'");
                        while ($donnees = $reponse->fetch()){
                            $_SESSION['user'] = $donnees['user'];
                            $_SESSION['id_user'] = $donnees['id'];
                        }

                        //set d'une variable pour utiliser l'id de l'user dans requêtes SQL
                        $id_user = $_SESSION['id_user'];

                        //recherche nbr de prjts
                        //SELECT COUNT(*) FROM projets WHERE id_chef_projet = '$id_user'
                        $reponse2 = $bdd->query("SELECT (SELECT COUNT(*) FROM projets WHERE id_chef_projet = '$id_user') + (SELECT COUNT(*) FROM associer WHERE id_associer = '$id_user') AS \"nbr\"");
                        while ($row = $reponse2->fetch()){
                            $rowfin = $row['nbr'];
                            $_SESSION['nb_prjt'] = $row['nbr'];
                        }
                    ?>
                    
                    <p1 class="text-center">Heureux de vous voir <?php echo $_SESSION['user']; //echo $_SESSION['email'] ?> !</p1></br>
                    <p2 class="text-center">Quel est votre projet pour aujourd'hui ? </p2> </br> </br>

                    <button class="btn_creer_prjt" type="submit" formaction="nouveau_projet.php" >Créer un nouveau projet</button> </br>
                    
                    <?php 
                    if ($_SESSION['nb_prjt'] > 0) { ?> 
                        <p3 class="text-center">Continuer un projet : </p3> 
                        <select id="id_proj" name="id_projet">
                                <?php
                                // 
                                //SELECT nom_projet, id_projet FROM projets WHERE id_chef_projet = '$id_user'
                                $reponse3 = $bdd->query("SELECT p.nom_projet, p.id_projet FROM projets p LEFT JOIN associer a ON p.id_projet = a.id_projet WHERE p.id_chef_projet = '$id_user' OR a.id_associer = '$id_user'");
                                while ($nprojet = $reponse3->fetch()){
                                    ?> <option value="<?php echo $nprojet['id_projet'] ?>"> <?php echo $nprojet['nom_projet'] ?></option> <?php 
                                }
                                ?>
                        </select>                        
                        <input class="bouton_continuer_prjt" type="submit" name="accueil" value="Valider">
                    <?php }  ?>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
