-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 22 fév. 2021 à 08:14
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ganttech`
--

-- --------------------------------------------------------

--
-- Structure de la table `associer`
--

DROP TABLE IF EXISTS `associer`;
CREATE TABLE IF NOT EXISTS `associer` (
  `id_assoc` int(11) NOT NULL AUTO_INCREMENT,
  `id_projet` int(11) NOT NULL,
  `id_chef` int(11) NOT NULL,
  `id_associer` int(11) NOT NULL,
  PRIMARY KEY (`id_assoc`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `associer`
--

INSERT INTO `associer` (`id_assoc`, `id_projet`, `id_chef`, `id_associer`) VALUES
(2, 31, 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mdp` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `login`
--

INSERT INTO `login` (`id`, `user`, `email`, `mdp`) VALUES
(1, 'admin', 'admin@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
(7, 'test', 'test@gmail.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08'),
(6, 'TestCrypta', 'ttt@tt', '3b6f52aa7955346fda8417c32546e3c0b5ddf785b62b88d0d83ea550341045b6');

-- --------------------------------------------------------

--
-- Structure de la table `ordre`
--

DROP TABLE IF EXISTS `ordre`;
CREATE TABLE IF NOT EXISTS `ordre` (
  `id_ordre` int(11) NOT NULL AUTO_INCREMENT,
  `id_projet` int(11) NOT NULL,
  `id_tache` int(11) NOT NULL,
  `prerequis` int(11) DEFAULT NULL,
  `tempsAvant` int(11) NOT NULL,
  PRIMARY KEY (`id_ordre`)
) ENGINE=MyISAM AUTO_INCREMENT=234 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ordre`
--

INSERT INTO `ordre` (`id_ordre`, `id_projet`, `id_tache`, `prerequis`, `tempsAvant`) VALUES
(108, 1, 8, 7, 13),
(107, 1, 7, 4, 7),
(106, 1, 9, 6, 11),
(105, 1, 6, 5, 9),
(104, 1, 5, 3, 6),
(103, 1, 4, 2, 3),
(102, 1, 3, 2, 3),
(101, 1, 2, 1, 1),
(100, 1, 1, NULL, 0),
(216, 21, 91, NULL, 10),
(142, 19, 45, NULL, 8),
(141, 19, 44, NULL, 7),
(140, 19, 43, NULL, 6),
(139, 19, 42, NULL, 7),
(138, 19, 41, NULL, 6),
(137, 19, 40, NULL, 5),
(136, 19, 39, NULL, 4),
(135, 19, 38, NULL, 3),
(134, 19, 37, NULL, 3),
(133, 19, 36, NULL, 1),
(132, 19, 35, NULL, 0),
(161, 20, 47, NULL, 0),
(162, 20, 48, NULL, 1),
(163, 20, 49, NULL, 3),
(164, 20, 50, NULL, 3),
(165, 20, 51, NULL, 4),
(166, 20, 52, NULL, 5),
(167, 20, 53, NULL, 5),
(168, 20, 54, NULL, 6),
(215, 21, 90, NULL, 9),
(214, 21, 89, NULL, 8),
(213, 21, 88, NULL, 8),
(212, 21, 87, NULL, 6),
(211, 21, 85, NULL, 5),
(210, 21, 86, NULL, 4),
(209, 21, 83, NULL, 3),
(208, 21, 84, NULL, 3),
(207, 21, 82, NULL, 2),
(206, 21, 81, NULL, 1),
(205, 21, 80, NULL, 0),
(217, 21, 92, NULL, 10),
(218, 21, 93, NULL, 12),
(219, 25, 94, NULL, 0),
(220, 25, 95, NULL, 3),
(221, 25, 96, NULL, 8),
(222, 26, 97, NULL, 0),
(223, 26, 98, NULL, 2),
(224, 26, 99, NULL, 5),
(225, 27, 100, NULL, 0),
(226, 27, 101, NULL, 2),
(227, 28, 102, NULL, 0),
(228, 28, 103, NULL, 2),
(229, 29, 104, NULL, 0),
(230, 29, 105, NULL, 2),
(231, 31, 108, NULL, 0),
(232, 31, 109, NULL, 2),
(233, 31, 110, NULL, 5);

-- --------------------------------------------------------

--
-- Structure de la table `projets`
--

DROP TABLE IF EXISTS `projets`;
CREATE TABLE IF NOT EXISTS `projets` (
  `id_projet` int(11) NOT NULL AUTO_INCREMENT,
  `id_chef_projet` int(11) NOT NULL,
  `nom_projet` varchar(100) NOT NULL,
  `date_deb` date NOT NULL,
  `nom_img` varchar(100) NOT NULL,
  `taille_img` int(11) NOT NULL,
  `type_img` varchar(20) NOT NULL,
  `bin_img` longblob NOT NULL,
  PRIMARY KEY (`id_projet`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `projets`
--

INSERT INTO `projets` (`id_projet`, `id_chef_projet`, `nom_projet`, `date_deb`, `nom_img`, `taille_img`, `type_img`, `bin_img`) VALUES
(1, 2, 'Premier Projet !', '2020-10-16', '', 0, '', ''),
(20, 1, 'Exemple', '2020-12-17', '', 0, '', ''),
(28, 1, 'Test Associer', '2021-02-03', '', 0, '', ''),
(25, 1, 'TEstCreaPrjt', '2021-01-27', '', 0, '', ''),
(21, 1, 'Vote', '2020-11-18', '', 0, '', ''),
(19, 1, 'Ganttech', '2020-10-18', '', 0, '', ''),
(26, 1, 'TestTaches', '2021-01-31', '', 0, '', ''),
(31, 7, 'Test Associer Admin', '2021-02-03', '', 0, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `taches`
--

DROP TABLE IF EXISTS `taches`;
CREATE TABLE IF NOT EXISTS `taches` (
  `id_tache` int(11) NOT NULL AUTO_INCREMENT,
  `id_projet` int(11) NOT NULL,
  `tache` text NOT NULL,
  `ordre` int(11) DEFAULT NULL,
  `nb_jour` int(3) NOT NULL,
  `attribution` text NOT NULL,
  `prerequis` varchar(200) NOT NULL,
  PRIMARY KEY (`id_tache`)
) ENGINE=MyISAM AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `taches`
--

INSERT INTO `taches` (`id_tache`, `id_projet`, `tache`, `ordre`, `nb_jour`, `attribution`, `prerequis`) VALUES
(1, 1, 'Idée', 1, 1, 'Baptiste, Christophe', ''),
(2, 1, 'Croquis', 2, 2, 'Baptiste, Christophe', '1'),
(3, 1, 'Conception DB', 3, 1, 'Baptiste', '2'),
(4, 1, 'Acceuil', 4, 2, 'Christophe', '2'),
(5, 1, 'Login', 5, 2, 'Baptiste', '3'),
(6, 1, 'Register', 6, 2, 'Baptiste', '5'),
(7, 1, 'Page créa projet', 8, 2, 'Christophe', '4'),
(8, 1, 'Relier toutes les pages', 9, 2, 'Baptiste, Christophe, Timothy', '7'),
(9, 1, 'Modification Gantt', 7, 4, 'Timothy', '6'),
(45, 19, 'Modification des taches Gantt', 11, 5, 'Timothy', ''),
(48, 20, 'Modelisation', 2, 2, 'Jean, Paul', ''),
(44, 19, 'Algorithme Gantt', 9, 4, 'Baptiste', ''),
(43, 19, 'Formulaire creation projet', 10, 5, 'Christophe', ''),
(42, 19, 'Hachage des mdp', 8, 1, 'Timothy', ''),
(40, 19, 'Register', 6, 1, 'Baptiste', ''),
(41, 19, 'Explication projet', 7, 1, 'Baptiste, Christophe, Timothy', ''),
(39, 19, 'Login', 5, 1, 'Baptiste', ''),
(37, 19, 'Conception DB', 3, 1, 'Baptiste', ''),
(38, 19, 'Accueil', 4, 2, 'Christophe', ''),
(36, 19, 'Modelisation, mockup', 2, 2, 'Baptiste, Christophe', ''),
(35, 19, 'Idee', 1, 1, 'Baptiste, Christophe', ''),
(47, 20, 'Idee', 1, 1, 'Jean, Paul', ''),
(49, 20, 'Accueil', 3, 1, 'Paul', ''),
(50, 20, 'Base de donnee', 4, 1, 'Jean', ''),
(51, 20, 'Mise en commun', 5, 1, 'Jean, Paul', ''),
(52, 20, 'Creation des comptes', 6, 1, 'Paul', ''),
(53, 20, 'Page de profil', 7, 1, 'Jean', ''),
(54, 20, 'Mise en commun', 8, 3, 'Jean, Paul', ''),
(90, 21, 'Login', 11, 1, 'Corentin', ''),
(89, 21, 'Creation des votes', 10, 2, 'Thibault', ''),
(87, 21, 'Mise en place du MVC', 8, 2, 'Corentin, Thibault', ''),
(88, 21, 'Register', 9, 1, 'Corentin', ''),
(86, 21, 'Creation de la BDD', 6, 1, 'Thibault', ''),
(84, 21, 'Mise en place du MLD', 4, 1, 'Thibault', ''),
(85, 21, 'Repartition des taches', 7, 1, 'Corentin, Thibault', ''),
(83, 21, 'Cahier des charges', 5, 2, 'Corentin', ''),
(82, 21, 'Definition des fonctionnalite', 3, 1, 'Corentin, Thibault', ''),
(81, 21, 'Recherche de solution', 2, 1, 'Corentin, Thibault', ''),
(80, 21, 'Idee du projet', 1, 1, 'Corentin, Thibault', ''),
(91, 21, 'Affichage des votes', 12, 1, 'Thibault', ''),
(92, 21, 'Mise en commun', 13, 2, 'Corentin, Thibault', ''),
(93, 21, 'Rapport', 14, 3, 'Corentin, Thibault', ''),
(94, 25, 'Premiere tache', 1, 3, 'Jean, Bert', ''),
(95, 25, 'La suite', 2, 5, 'Jean, Bert', ''),
(96, 25, 'La fin', 3, 8, 'Jean, Bert', ''),
(97, 26, 'Premiere tache', 1, 2, 'Moi', ''),
(98, 26, '', 2, 3, 'Moi', ''),
(99, 26, 'Troisieme', 3, 2, 'Moi', ''),
(100, 27, 'Premiere tache', 1, 2, 'Moi', ''),
(101, 27, 'Troisieme', 3, 3, 'Moi', ''),
(102, 28, 'Idee', 1, 2, 'Moi', ''),
(103, 28, 'Travail', 2, 3, 'Moi', ''),
(104, 29, 'Idee', 1, 2, 'Moi', ''),
(105, 29, 'Suite', 2, 3, 'Moi', ''),
(106, 30, 'Idee', 1, 2, 'Moi', ''),
(107, 30, 'Suite', 2, 3, 'Moi', ''),
(108, 31, 'test', 1, 2, 'Moi', ''),
(109, 31, 'associer', 2, 3, 'Moi', ''),
(110, 31, 'admin', 3, 2, 'Moi', '');

-- --------------------------------------------------------

--
-- Structure de la table `temps_travail`
--

DROP TABLE IF EXISTS `temps_travail`;
CREATE TABLE IF NOT EXISTS `temps_travail` (
  `id_temps_travail` int(11) NOT NULL AUTO_INCREMENT,
  `nom_travailleur` varchar(150) NOT NULL,
  `temps_travail` int(11) NOT NULL,
  `id_projet` int(11) NOT NULL,
  `id_tache` int(11) NOT NULL,
  PRIMARY KEY (`id_temps_travail`)
) ENGINE=MyISAM AUTO_INCREMENT=443 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `temps_travail`
--

INSERT INTO `temps_travail` (`id_temps_travail`, `nom_travailleur`, `temps_travail`, `id_projet`, `id_tache`) VALUES
(335, 'Jean', 1, 20, 47),
(291, 'Timothy', 6, 19, 34),
(306, 'Timothy', 5, 19, 45),
(305, 'Baptiste', 4, 19, 44),
(304, 'Christophe', 5, 19, 43),
(303, 'Timothy', 1, 19, 42),
(302, 'Timothy', 1, 19, 41),
(301, 'Christophe', 1, 19, 41),
(300, 'Baptiste', 1, 19, 41),
(299, 'Baptiste', 1, 19, 40),
(298, 'Baptiste', 1, 19, 39),
(297, 'Christophe', 3, 19, 38),
(296, 'Baptiste', 1, 19, 37),
(295, 'Christophe', 2, 19, 36),
(294, 'Baptiste', 2, 19, 36),
(293, 'Christophe', 1, 19, 35),
(292, 'Baptiste', 1, 19, 35),
(260, 'Timothy', 2, 1, 8),
(259, 'Christophe', 2, 1, 8),
(258, 'Baptiste', 2, 1, 8),
(257, 'Christophe', 2, 1, 7),
(256, 'Timothy', 4, 1, 9),
(255, 'Baptiste', 2, 1, 6),
(254, 'Baptiste', 2, 1, 5),
(253, 'Christophe', 2, 1, 4),
(252, 'Baptiste', 1, 1, 3),
(251, 'Christophe', 2, 1, 2),
(250, 'Baptiste', 2, 1, 2),
(249, 'Christophe', 1, 1, 1),
(248, 'Baptiste', 1, 1, 1),
(336, 'Paul', 1, 20, 47),
(337, 'Jean', 2, 20, 48),
(338, 'Paul', 2, 20, 48),
(339, 'Paul', 1, 20, 49),
(340, 'Jean', 1, 20, 50),
(341, 'Jean', 1, 20, 51),
(342, 'Paul', 1, 20, 51),
(343, 'Paul', 1, 20, 52),
(344, 'Jean', 1, 20, 53),
(345, 'Jean', 3, 20, 54),
(346, 'Paul', 3, 20, 54),
(423, 'Corentin', 3, 21, 93),
(422, 'Thibault', 2, 21, 92),
(421, 'Corentin', 2, 21, 92),
(420, 'Thibault', 1, 21, 91),
(419, 'Corentin', 2, 21, 90),
(418, 'Thibault', 2, 21, 89),
(417, 'Corentin', 1, 21, 88),
(416, 'Thibault', 2, 21, 87),
(415, 'Corentin', 2, 21, 87),
(414, 'Thibault', 1, 21, 85),
(413, 'Corentin', 1, 21, 85),
(412, 'Thibault', 1, 21, 86),
(411, 'Corentin', 2, 21, 83),
(410, 'Thibault', 1, 21, 84),
(409, 'Thibault', 1, 21, 82),
(408, 'Corentin', 1, 21, 82),
(407, 'Thibault', 1, 21, 81),
(406, 'Corentin', 1, 21, 81),
(405, 'Thibault', 1, 21, 80),
(404, 'Corentin', 1, 21, 80),
(424, 'Thibault', 3, 21, 93),
(425, 'Jean', 3, 25, 94),
(426, 'Bert', 3, 25, 94),
(427, 'Jean', 5, 25, 95),
(428, 'Bert', 5, 25, 95),
(429, 'Jean', 8, 25, 96),
(430, 'Bert', 8, 25, 96),
(431, 'Moi', 2, 26, 97),
(432, 'Moi', 3, 26, 98),
(433, 'Moi', 2, 26, 99),
(434, 'Moi', 2, 27, 100),
(435, 'Moi', 3, 27, 101),
(436, 'Moi', 2, 28, 102),
(437, 'Moi', 3, 28, 103),
(438, 'Moi', 2, 29, 104),
(439, 'Moi', 3, 29, 105),
(440, 'Moi', 2, 31, 108),
(441, 'Moi', 3, 31, 109),
(442, 'Moi', 2, 31, 110);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
