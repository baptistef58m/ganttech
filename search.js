const searchWrapper = document.querySelector(".recherche");
const inputBox = searchWrapper.querySelector("input");
const suggBox = searchWrapper.querySelector(".autocomp");

inputBox.onkeyup = (e)=>{
    let datas = e.target.value; //user enetered data
    let liste = [];
    if(datas){
        liste = suggestions.filter((data)=>{
            //filtering array value and user characters to lowercase and return only those words which are start with user enetered chars
            return data.toLocaleLowerCase().startsWith(datas.toLocaleLowerCase()); 
        });
        liste = liste.map((data)=>{
            // passing return data inside li tag
            return data = '<li>'+ data +'</li>';
        });
        searchWrapper.classList.add("active"); //show autocomplete box
        showSuggestions(liste);
        let allList = suggBox.querySelectorAll("li");
        for (let i = 0; i < allList.length; i++) {
            allList[i].setAttribute("onclick", "select(this)");
        }
    }else{
        searchWrapper.classList.remove("active"); //hide autocomplete box
    }
}

function select(element){
    let selectData = element.textContent;
    inputBox.value = selectData;
    
    searchWrapper.classList.remove("active");
}

function showSuggestions(list){
    let listData;
    if(!list.length){
        //val = inputBox.value;
        //listData = '<li>'+ val +'</li>';
    }else{
        listData = list.join('');
    }
    suggBox.innerHTML = listData;
}
