<?php require_once "controle_donne.php"; require_once "queryGantt.php"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Gantt</title>
    <link rel="stylesheet" href="gantt.css">
</head>
<body>
    <style type="text/css">
        .hiddenDiv
        {
            display:none;
        }
        .buutonHidder:hover > .hiddenDiv
        {
            display: inherit;
        }
  </style>
  <img src="img.php?id= <?= $_SESSION['id_projet'] ?>" />
    <form action="gantt.php" method="POST" autocomplete="">
    <div class="container">
        <div class="row">
            <div class="gantt">
                    <?php
                    //création de la liste vide
                    $itemArrayList = array();
                    $TT = array();
                    //initialisation de la liste
                    $itemArrayList = queryGantt::ListTache($con);
                    ?>
                    <table>
                        <tr>
                            <th scope="prems">Tache</th>
                            <th scope="prems">Durée (en j)</th>
                            <th scope="prems">Attribution</th>
                            <th colspan="200" scope="prems">Dates</th>
                        </tr>

                    <?php 
                    for ( $i = 0; $i < count($itemArrayList); $i++){ 
                        $o = $i + 1;?>
                        <tr><td class="buutonHidder"><?php echo $itemArrayList[$i]->nom; ?>
                        <div class="hiddenDiv"> 
                        <form action=modifier.php> 
                        <input type="submit" name='modif' value="<?= $o ?>"/> 
                        </form> 
                        </div> </td>
                        <td><?php echo $duree = $itemArrayList[$i]->nb_jour; ?>  </td>
                        <td><?php echo $itemArrayList[$i]->attribution; ?>  </td> <?php

                        //on check les grps de travail :
                        $personne = explode(", ", $itemArrayList[$i]->attribution);
                        $nbrPersonnes = str_word_count($itemArrayList[$i]->attribution);
                        $personnesActuelles = array();

                        //case vide avant les jours de travail
                        for($tour1 = 0; $tour1 < $nbrPersonnes; $tour1++){
                            queryGantt::addTpsTravail($bdd, $personne[$tour1], $itemArrayList[$i]->id, $itemArrayList[$i]->nb_jour);
                            
                            //récup tps travail :
                            $tpsTravail[$tour1] = queryGantt::tpsTotalTravail($bdd, $personne[$tour1], $itemArrayList[$i]->id, $itemArrayList[$i]->prerequis);
                            $tour2 = $tour1;
                            //$tour2--;
                            if ($tpsTravail[$tour1] == null)
                                $TT[$i] = 0;
                            else if ($tour1 == 0)
                                $TT[$i] = $tpsTravail[$tour1];
                            else if($tpsTravail[$tour1] > $tpsTravail[$tour2--])
                                $TT[$i] = $tpsTravail[$tour1];
                            //mise en ordre
                            queryGantt::miseEnOrdre($bdd, $itemArrayList[$i]->id, $itemArrayList[$i]->prerequis, $TT[$i]);
                        }
                        //cases avant temps de travail
                        for($k = $TT[$i]; $k > 0; $k--){ ?>
                            <td scope="sansdate"></td>  <?php 
                        }
                        //remplissage des cases de travail
                        for($j = $duree; $j > 0; $j--){ ?>
                            <td scope="date"><?php echo 'Tps' ?></td>    <?php 
                        } ?> 
                        <!-- remplissage de fin de ligne --> 
                        <td colspan = "1000" scope="sansdate"></td> <?php 
                    }
                    ?>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
