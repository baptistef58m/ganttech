<?php 
    session_start();
    require_once "connexion.php"; require_once "queryGantt.php";
    $email = "";
    $user = "";
    $id = "";
    $errors = array();

    function crypta($mdp) {
        $nmdp = hash("sha256", $mdp);
        return $nmdp;
    }

    //si on clic sur créer compte
    if(isset($_POST['register'])){
        $user = mysqli_real_escape_string($con, $_POST['user']);
        $email = mysqli_real_escape_string($con, $_POST['email']);
        $mdp = mysqli_real_escape_string($con, $_POST['mdp']);
        $cmdp = mysqli_real_escape_string($con, $_POST['cmdp']);
        if($mdp !== $cmdp){
            $errors['mdp'] = "Le mot de passe de confirmation ne correspond pas à votre mot de passe !";
        }
        $email_check = "SELECT * FROM login WHERE email = '$email'";
        $result = mysqli_query($con, $email_check);
        if(mysqli_num_rows($result) > 0){
            $errors['email'] = "L'adresse mail rentrée est déjà enregistrée !";
        }
        if(count($errors) === 0){
            $mdpcrypter = crypta($mdp);
            $_SESSION['user']=$user;
            $insert_data = "INSERT INTO login (user, email, mdp )
                            values('$user', '$email', '$mdpcrypter')";
            $data_check = mysqli_query($con, $insert_data);
            if($data_check){
                header('location: login.php');
                exit();
            }
        }
    }

    //bouton se connecter
    if(isset($_POST['login'])){
        $email = mysqli_real_escape_string($con, $_POST['email']);
        $mdp = mysqli_real_escape_string($con, $_POST['mdp']);
        $check_email = "SELECT * FROM login WHERE email = '$email'";
        $result = mysqli_query($con, $check_email);
        if(mysqli_num_rows($result) > 0){
            $mdpcrypt = crypta($mdp);
            $check_compte = "SELECT * FROM login WHERE email = '$email' AND mdp ='$mdpcrypt'";
            $result2 = mysqli_query($con, $check_compte);
            if(mysqli_num_rows($result2) > 0){
                //c'est bon -> envoyer sur la page d'acceuil
                $_SESSION['email']=$email;
                $query = "SELECT id FROM login WHERE email = '$email'";
                $result3 = mysqli_query($con, $query);
                $recup_id = mysqli_fetch_array($result3);
                $_SESSION['id_user']= $recup_id['id'];
                header('location: accueil.php');
            }else{
                $errors['email'] = "Adresse mail ou mot de passe incorect !";
            }
        }else{
            $errors['email'] = "Il semblerai que vous n'avez pas encore de compte, créez en un maintenant !";
        }
    }

    //
    if(isset($_POST['accueil'])){
        $id_projet = mysqli_real_escape_string($con, $_POST['id_projet']);
        $_SESSION['id_projet'] = $id_projet;
        header('location: gantt.php');
    }

    //nouveau projet
    if(isset($_POST['creer_prjt'])){
        $id_user = $_SESSION['id_user'];
        $nom = $_POST['nom_projet'];
        $date = $_POST['date_deb'];
        $l = $_POST["ligne"];

        $assoc  = $_POST["associer"];

        $id_prjt = queryGantt::CreatePrjt($con, $bdd, $id_user, $nom, $date);

        for($i = 1; $i < $l; $i++){
            $tache = $_POST['Text'.$i.''];
            
            if(!empty($tache)){
                $liste = $_POST['Liste'.$i.''];
                $attribut = $_POST['Attribut'.$i.''];
                $prereq = $_POST['Prerequis'.$i.''];
            
                queryGantt::AddTache($bdd, $id_prjt, $tache, $i, $liste, $attribut, $prereq);
            }
        }

        if (!empty($assoc)){
            //vérif qu'il existe bien en chopant son id
            $query = "SELECT id FROM login WHERE user = '$assoc'";
            $result = mysqli_query($con, $query);
            $recup_id = mysqli_fetch_array($result);
            $id_assoc = $recup_id['id'];
            //add au projet
            $insert_data = "INSERT INTO associer(id_projet, id_chef, id_associer) VALUES ('$id_prjt', '$id_user', '$id_assoc')";
            $data_check = mysqli_query($con, $insert_data);
        }

        $_SESSION['id_projet'] = $id_prjt;
        header('location: gantt.php');
    }

?>