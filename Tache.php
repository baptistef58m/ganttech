<?php

    class Tache {

        public $id ;
        public $idPrjt;
        public $nom;
        public $ordre;
        public $nb_jour;
        public $attribution;
        public $prerequis;

        public function __construct($i, $iP, $n, $o, $nb, $attribu, $prereq){
			$this->id = $i;
            $this->idPrjt = $iP;
            $this->nom = $n;
            $this->ordre = $o;
            $this->nb_jour = $nb;
            $this->attribution = $attribu;
            $this->prerequis = $prereq;
        }

        public function getId() {
            return $id;
        }
        public function setId($id) {
            $this->id = $id;
        }

        public function getIdPrjt() {
            return $idPrjt;
        }
        public function setIdPrjt($idPrjt) {
            $this->idPrjt = $idPrjt;
        }

        public function getNom() {
            return $nom;
        }
        public function setNom($nom) {
            $this->nom = $nom;
        }

        public function getOrdre() {
            return $ordre;
        }
        public function setOrdre($ordre) {
            $this->ordre = $ordre;
        }

        public function getNbJour() {
            return $nb_jour;
        }
        public function setNbJour($nb_jour) {
            $this->nb_jour = $nb_jour;
        }

        public function getAttribution() {
            return $attribution;
        }
        public function setAttribution($attribution) {
            $this->attribution = $attribution;
        }

        public function getPrerequis() {
            return $prerequis;
        }
        public function setPrerequis($prerequis) {
            $this->prerequis = $prerequis;
        }

        public function __toString() {
            return "{".
                    "id_tache : $this->id, ".
                    "id_projet : $this->idPrjt, ".
                    "nom : $this->nom, ".
                    "ordre : $this->ordre, ".
                    "nb_jour : $this->nb_jour, ".
                    "attribution : $this->attribution, ".
                    "prerequis : $this->prerequis ".
                    '}</br>';
        }
    }
?>