<?php   require_once "controle_donne.php";
        require_once "connexion.php";
        require_once "Tache.php";

    class queryGantt {

        //faire la liste des tâches
        public static function ListTache($con) {
            $list = array();
            
            $id_prjt = $_SESSION['id_projet'];
            //SELECT * FROM taches t INNER JOIN ordre o ON t.id_tache = o.id_tache WHERE t.id_projet = $id_prjt GROUP BY t.tache ORDER BY o.prerequis, o.tempsAvant, t.nb_jour
            $query = ("SELECT id_tache, id_projet, tache, ordre, nb_jour, attribution, prerequis FROM taches WHERE id_projet = $id_prjt ORDER BY ordre");
            $query2 = ("SELECT * FROM taches t INNER JOIN ordre o ON t.id_tache = o.id_tache WHERE t.id_projet = $id_prjt GROUP BY t.tache ORDER BY o.prerequis, o.tempsAvant, t.nb_jour");
            $result = mysqli_query($con, $query) or die("Erreur lors du chargement des données, réessayez.");     
            while($ligne = mysqli_fetch_array($result)){
                $tache = new Tache($ligne['id_tache'], $ligne['id_projet'], $ligne['tache'], $ligne['ordre'], $ligne['nb_jour'], $ligne['attribution'], $ligne['prerequis']);
                array_push($list, $tache);
            }
            //print_r($list);
            return $list;
        }

        public static function suggestPersonne($con, $id_chef) {
            $list = array();

            $query = ("SELECT user FROM login WHERE id != '$id_chef'");
            $result = mysqli_query($con, $query) or die("Erreur lors du chargement des données, réessayez.");     
            while($ligne = mysqli_fetch_array($result)){
                array_push($list, $ligne['user']);
            }
            return $list;
        }

        //fct créer un projet
        static function CreatePrjt($con, $bdd, $id_chef, $nom, $date_deb) {

            $query = "INSERT INTO projets(id_chef_projet, nom_projet, date_deb) VALUES (?, ?, ?);";
            try {
                //création du projet dans la DB
                $prep = $bdd->prepare($query);
                $prep->bindValue(1, $id_chef, PDO::PARAM_INT);
                $prep->bindValue(2, $nom, PDO::PARAM_STR);
                $prep->bindValue(3, $date_deb, PDO::PARAM_STR);
                $prep->execute();
    
                //récup de l'id du projet pour la suite
                $query2 = "SELECT id_projet FROM projets WHERE id_chef_projet = $id_chef AND nom_projet = \"".$nom."\";";
                $result = mysqli_query($con, $query2) or die("Erreur lors de la création du projet, réessayez.");
                while($ligne = mysqli_fetch_array($result)){
                    $res = $ligne['id_projet'];
                }
                return $res;
            } catch ( Exception $e ) {
                die("erreur dans la requete ".$e->getMessage()); 
            }
        }

        // ajouter une tâche
        static function AddTache($bdd, $id_projet, $tache, $ordre, $nb_jour, $attribution, $prerequis) {
            
            $query = "INSERT INTO taches(id_projet, tache, ordre, nb_jour, attribution, prerequis) VALUES (?, ?, ?, ?, ?, ?)";
            try {
                $prep = $bdd->prepare($query);
                $prep->bindValue(1, $id_projet, PDO::PARAM_INT);
                $prep->bindValue(2, $tache, PDO::PARAM_STR);
                $prep->bindValue(3, $ordre, PDO::PARAM_INT);
                $prep->bindValue(4, $nb_jour, PDO::PARAM_INT);
                $prep->bindValue(5, $attribution, PDO::PARAM_STR);
                $prep->bindValue(6, $prerequis, PDO::PARAM_STR);
                $prep->execute();
    
            } catch ( Exception $e ) {
                die("erreur dans la requete ".$e->getMessage()); 
            }
        }

        static function tpsTotalTravail($bdd, $nom, $id_tache, $prerequis){

            $id_prjt = $_SESSION['id_projet'];

            $query ="SELECT SUM(temps_travail) AS total FROM temps_travail WHERE id_projet = $id_prjt AND nom_travailleur = \"".$nom."\" AND id_tache < \"".$id_tache."\";";
            $SQL = $bdd->query($query);
            $data = $SQL->fetch();
            $somme = $data['total'];

            //$query2 = "SELECT SUM(temps_travail) AS total FROM temps_travail tt INNER JOIN taches t ON tt.id_tache = t.id_tache WHERE t.id_projet = $id_prjt AND t.ordre < \"".$prerequis."\" GROUP BY t.prerequis ORDER BY t.prerequis DESC;";
            //$query2 = "SELECT tempsAvant FROM ordre WHERE prerequis = \"".$prerequis--."\"";
            //$query2 = "SELECT SUM(tempsAvant) AS total FROM ordre o INNER JOIN temps_travail t ON t.id_tache = o.id_tache WHERE prerequis = \"".$prerequis--."\" GROUP BY t.id_tache;";
            $query2 = "SELECT tempsAvant FROM ordre o INNER JOIN taches t ON o.id_tache = t.id_tache WHERE o.id_projet = $id_prjt AND o.id_tache = (SELECT id_tache FROM taches WHERE ordre = \"".$prerequis."\")";
            $SQL = $bdd->query($query2);
            $data2 = $SQL->fetch();
            $somme2 = $data2['tempsAvant'];
            $somme2--;
            //echo $nom.' Somme : '.$somme.' Somme 2 : '.$somme2."</br>"; 
            
            //peut être pour préreq
            //SELECT tempsAvant FROM ordre o INNER JOIN taches t ON o.id_tache = t.id_tache WHERE o.id_projet = 1 AND o.id_tache = (SELECT id_tache FROM taches WHERE ordre = 6)

            if($somme2 > $somme) {
                /*$idTache = $id_tache;
                $idTache--;
                $somme3 = $somme2;
                $somme3--;
                $query3 = "UPDATE ordre SET tempsAvant = tempsAvant + $somme3 WHERE id_projet = $id_prjt AND id_tache = \"".$idTache."\";";
                echo $query3;
                $SQL = $bdd->query($query3);*/
                echo $nom.' Somme : '.$somme.' Somme 2 : '.$somme2."</br>"; 

                $somme = $somme2;
            }

            //echo $nom.' Somme : '.$somme."</br>"; 

            return $somme;
        }

        static function addTpsTravail($bdd, $nom, $id_tache, $nb_jour)
        {
            $id_prjt = $_SESSION['id_projet'];

            $query = "SELECT id_temps_travail FROM temps_travail WHERE nom_travailleur = \"".$nom."\" AND id_tache = $id_tache AND id_projet = $id_prjt;";
            $SQL = $bdd->query($query);
            $id_tps_travail = $SQL->fetch();
        
            if ($id_tps_travail == null){
                $query2 = "INSERT INTO temps_travail(nom_travailleur, temps_travail, id_projet, id_tache) 
                                VALUES (\"".$nom."\", \"".$nb_jour."\", \"".$id_prjt."\", \"".$id_tache."\");";
                $SQL = $bdd->prepare($query2);
                $SQL->execute();
            }
            /*else {
                $query2 = "UPDATE temps_travail SET nom_travailleur = \"".$nom."\", temps_travail = \"".$nb_jour."\" WHERE id_projet = \"".$id_prjt."\" AND id_tache = \"".$id_tache."\";";
                $SQL = $bdd->prepare($query2);
                $SQL->execute();
            }*/       
        }

        static function updateTpsTravail($bdd, $nom, $id_tache, $nb_jour)
        {
            $id_prjt = $_SESSION['id_projet'];

            $query2 = "UPDATE temps_travail SET temps_travail = \"".$nb_jour."\" WHERE id_projet = \"".$id_prjt."\" AND id_tache = \"".$id_tache."\" AND nom_travailleur = \"".$nom."\";";
            $SQL = $bdd->prepare($query2);
            $SQL->execute();       
        }

        static function miseEnOrdre($bdd, $id_tache, $prerequis, $TT)
        {
            /**
             * Check le temps de travail de la personne avant et le temps de travail passé des taches du prérequis
             * → regarder la plus grande valeur qui deviendra TT
             * 
             */
            $id_prjt = $_SESSION['id_projet'];
            
            //On va check la valeur de TT pour que ça soit bien aligné avec les prérequis
            $query = "SELECT SUM(temps_travail) AS total FROM temps_travail tt INNER JOIN taches t ON tt.id_tache = t.id_tache WHERE t.id_projet = $id_prjt AND t.ordre < \"".$prerequis."\";";
            $SQL = $bdd->query($query);
            $data = $SQL->fetch();
            $TT2 = $data['total'];

            if($TT2 > $TT) {
                $TT = $TT2;
            }

            //ajout dans la table ordre
            $query2 = "SELECT id_ordre FROM ordre WHERE id_projet = $id_prjt AND id_tache = $id_tache;";
            $SQL = $bdd->query($query2);
            $id_ordre = $SQL->fetch();
        
            if ($id_ordre == null){
                $query3 = "INSERT INTO ordre(id_projet, id_tache, prerequis, tempsAvant) VALUES(\"".$id_prjt."\", \"".$id_tache."\", \"".$prerequis."\", \"".$TT."\");"; 
                if ($prerequis == null)
                    $query3 = "INSERT INTO ordre(id_projet, id_tache, tempsAvant) VALUES(\"".$id_prjt."\", \"".$id_tache."\", \"".$TT."\");";   
                $SQL = $bdd->prepare($query3);
                $SQL->execute();
            }
            else {
                $query4 = "UPDATE ordre SET prerequis = \"".$prerequis."\", tempsAvant = \"".$TT."\" WHERE id_projet = \"".$id_prjt."\" AND id_tache = \"".$id_tache."\";"; 
                //echo $query4."</br>";
                $SQL = $bdd->prepare($query4);
                $SQL->execute();
            }
            //update
        }
    }
?>