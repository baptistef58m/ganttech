<?php require_once "controle_donne.php"; require_once "queryGantt.php"; 
if(isset($_POST["creer_prjt"])){
  $con = mysqli_connect('localhost', 'root', '', 'ganttech');
  $query=$pdo->prepare("insert into projets(nom_img, taille_img, type_img, bin_img) values (?, ?, ?, ?)");
  $query->execute(array($_FILES["image"]["name"], $_FILES["image"]["size"], $_FILES["image"]["type"], file_get_contents($_FILES["image"]["tmp_name"])));
}
?>
<!DOCTYPE html>
<html lang="fr">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width",initial-scale=1.0>
 <title>GantTech</title>
 <link rel="stylesheet" href="creat.css">
 </head>
 <body>
 <form action="controle_donne.php" method="post" name="frm">

 <p>BIENVENUE SUR LE GANTTECH, UNE APPLICATION QUI VA SE BASER SUR LA CREATION D'UN DIAGRAMME DE GANTT !</p>
    <p1>Nom du projet : <input name="nom_projet" placeholder="Nom de votre projet" size="40" type="text" maxlength="80"></br>
    Image du projet : <input type="file" name="image"/></p1>
    <label for="start">Date de début: <input type="date" id="start" name="date_deb" value="2021-01-01" min="01/09/2020" max="01/09/2290"></label>
    

    <!-- Donner l'accès à une personne-->
    <div class="forme">
        <div class="recherche">
            <p2>Donner l'accès à quelqu'un au projet : <input name="associer" placeholder="Ajouter quelqu'un au projet" size="40" type="text" maxlength="80" autocomplete="off">
                <div class="autocomp">
                    <!-- liste des personnes enregistré dans la DB -->
                </div></br>
            </p2>
        </div>
    </div>
    <p3>Attention toute personne ajoutée pourra modifier l'intégralité de votre projet. </br> Ajoutez seulement des personnes de confiance.</p3>
    <table class="tableau-style" id="lol">
        <thread>
            <tr>
                <th> </th>
                <th>Tâches</th>
                <th>Durée</th>
                <th>Attribution</th>
                <th>Prérequis</th>
            </tr>
        </thread>

        <body>
            <?php 
                $id_user = $_SESSION['id_user'];
                $nbligne = 1;
                $sugg = array();
                $sugg = queryGantt::suggestPersonne($con, $id_user);
            ?>
        
        <tr id="test">
            <td><?php echo $nbligne; ?></td>
            <td><input type="text" onblur=addRow() name=<?php echo "Text".$nbligne ; ?> placeholder="Intitulé de la tâche" size=30></td>
            <td><Select name=<?php echo "Liste".$nbligne ; ?> size="3">
                <?php for ($i = 1; $i < 200; $i++){ ?>
                    <option value=<?php echo $i; ?>><?php echo $i ; if($i == 1) {echo " jour"; } else { echo " jours"; }?></option>
                <?php } ?>
            </td>
            <td><input class="form-control" type="text" name=<?php echo "Attribut".$nbligne ; ?> 
                placeholder="Nom de/des personne(s) chargée de la tâche" size=30></td>
            <td><input class="form-control" type="text" name=<?php echo "Prerequis".$nbligne ; ?> 
                placeholder="Prérequis pour cette tâche"></td>
        </tr>                
            <script>
                let nbligneJS = 1;

                function addRow()
                {
                    nbligneJS++;

                    let newRow = document.all("lol").insertRow(-1);

                    let newline = newRow.insertCell();
 
                    newline.innerHTML = nbligneJS;

                    newline = newRow.insertCell();
                    newline.innerHTML = "<input type='text' onblur=addRow() name=Text"+nbligneJS+" placeholder='Intitulé de la tâche' size=30>";

                    newline = newRow.insertCell();
                    newline.innerHTML = "<Select name=Liste"+nbligneJS+" size='3'><?php for ($i = 1; $i < 200; $i++){ ?><option value=<?php echo $i; ?>><?php echo $i ; if($i == 1) {echo " jour"; } else { echo " jours"; }?></option><?php } ?>";

                    newline = newRow.insertCell();
                    newline.innerHTML = "<input class='form-control' type='text' name=Attribut"+nbligneJS+" placeholder='Nom de/des personne(s) chargée de la tâche' size=30>";
                
                    newline = newRow.insertCell();
                    newline.innerHTML = "<input class='form-control' type='text' name=Prerequis"+nbligneJS+" placeholder='Prérequis pour cette tâche'>";
                    
                    document.getElementById("nbLi").value = nbligneJS;
                }

            </script>
        </body>
    </table>
    <div class="accueil">
        <section class="home1">
            <h1>Veuillez valider votre choix juste ici.</h1>
            <!--a class="button" href="test2.html"><strong>Valider</strong></a -->
            <p4><input class="boutton" type="submit" name="creer_prjt" value="Valider"></p4>
            <input type="hidden" id="nbLi" name="ligne" value="">
        </section>
    </div>
    </form>

    <script>
        <?php
            echo "let suggestions = '".implode("<>", $sugg)."'.split('<>');"; ?>
            //alert(suggestions);
        </script>
    <script src="search.js"></script>
</body>
</html>
