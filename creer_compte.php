<?php require_once "controle_donne.php"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Page d'enregistrement</title>
    <link rel="stylesheet" href="creer_compte.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="register">
                <form action="creer_compte.php" method="POST" autocomplete="" class="rectangle">
                    <h2 class="text-center">Créer un compte</h2>
                    <p class="text-center">C'est simple et rapide !</p>
                    <?php
                    if(count($errors) == 1){
                        ?>
                        <div class="alert alert-danger text-center">
                            <?php
                            foreach($errors as $showerror){
                                echo $showerror;
                            }
                            ?>
                        </div>
                        <?php
                    }elseif(count($errors) > 1){
                        ?>
                        <div class="alert alert-danger">
                            <?php
                            foreach($errors as $showerror){
                                ?>
                                <li><?php echo $showerror; ?></li>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="group">
                        <input class="form-control" type="text" name="user" placeholder="Nom d'utilisateur" required value="<?php echo $user ?>">
                    </div>
                    <div class="group">
                        <input class="form-control" type="email" name="email" placeholder="Adresse mail" required value="<?php echo $email ?>">
                    </div>
                    <div class="group">
                        <input class="form-control" type="password" name="mdp" placeholder="Mot de passe" required>
                    </div>
                    <div class="group">
                        <input class="form-control" type="password" name="cmdp" placeholder="Confirmation du mot de passe" required>
                    </div>
                    <div class="group">
                        <input class="form-control button" type="submit" name="register" value="S'enregistrer !">
                    </div>
                    <div class="login">Vous êtes déjà membre ? <a href="login.php">Connectez-vous !</a></div>
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>
